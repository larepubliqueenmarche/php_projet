<?php
require_once __DIR__ . '/vendor/autoload.php';

use wishlist\modele\Item as Item;
use wishlist\modele\Liste as Liste;
use wishlist\modele\Utilisateur as Utilisateur;
use Illuminate\Database\Capsule\Manager as DB;

$db = new DB();

$db->addConnection(parse_ini_file('src/conf/conf.ini'));

$db->setAsGlobal();
$db->bootEloquent();

$idUser = Utilisateur::select('id_user','nom','prenom','mail','mot_de_passe','date_naissance','id_rang')->get();
?>
<table>
	<thead>
		<th><strong>Liste des Utilisateurs<strong></th>
	</thead>
		<tbody>
			<th>ID</th>
			<?php  foreach ($idUser as $users) {
				echo"<td>$users->id_user</td><br>";
				echo"<td>$users->nom</td>";
				echo"<td>$users->prenom</td>";
				echo"<td>$users->mail</td>";
				echo"<td>$users->mot_de_passe</td>";
				echo"<td>$users->date_naissance</td>";
				echo"<td>$users->id_rang</td>";
				echo "</tr>";
			}
			?>
		</tbody>
	</table>