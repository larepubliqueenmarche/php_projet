<?php
require_once __DIR__ . '/vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;
use wishlist\modele\Item as Item;
use wishlist\modele\Liste as Liste;
use wishlist\controller\UserController;
use wishlist\controller\ItemController;
$db = new DB();

$db->addConnection(parse_ini_file('src/conf/conf.ini'));

$db->setAsGlobal();
$db->bootEloquent();

$app = new \Slim\Slim();

function generationToken($nombreCaracteres){
        $string = "";
        $chaine = "a0b1c2d3e4f5g6h7i8j9klmnpqrstuvwxy123456789";
        srand((double)microtime()*1000000);
        for($i=0; $i<$nombreCaracteres; $i++){
            $string .= $chaine[rand()%strlen($chaine)];
        }
        return $string;
    }

$app->get('/',function(){
    $app = \Slim\Slim::getInstance();
    $rootUri = $app->request()->getRootUri();
    $creationutil = $rootUri . '/creationdecompte';
    $authenUti = $rootUri . '/gestion_liste';
    $goListe = $rootUri . '/liste';
    $goUpdate = $rootUri .'/updatelistetampon';
    $goPartage = $rootUri .'/partageurl';
    print
    <<< heredoc
    <h1>Accueil et Inscription</h1>
    <form action="$creationutil" method="POST">
    <link rel="stylesheet" type="text/css" href="styles.css">

      <input type="text" placeholder="Nom" name="nom" required>
      <input type="password" placeholder="Mot de passe" name="mot_de_passe">
       <input type="password" placeholder="Mot de passe bis" name="mot_de_passe_bis">
      <button type="submit">M'inscrire</button>
    </form>

    <h1>Authentification</h1>
    <form action="$authenUti" method="POST">

      <input type="text" placeholder="Nom" name="nom" required>
      <input type="password" placeholder="Mot de passe" name="mot_de_passe">
      <button type="submit">Me connecter</button>

    </form>

    <form action="$goListe" method="GET">
    <button type="submit">Ajouter liste</button>
    </form>

     <form action="$goUpdate" method="POST">
    <input type="select" placeholder="token de modification" name="token_modification">
    <button type="submit">Ajouter des items</button>
    </form>

    <form action="$goPartage" method="POST">
     <input type="select" placeholder="token de modification" name="token_modification">
     <button type="submit">Creer le lien de Partage</button>
heredoc;
});

$app->post('/creationdecompte',function(){
  $nom = $_POST['nom'];
  $mdp = $_POST['mot_de_passe'];
  $confirmer_mdp = $_POST['mot_de_passe_bis'];
  UserController::creationUtilisateur($nom,$mdp,$confirmer_mdp);
});


$app->post('/gestion_liste',function(){
     print
    <<< heredoc
       <!DOCTYPE html>
<html>
<head>
  <title>Connexion</title>
  <link rel="stylesheet" type="text/css" href="../css/styles.css">
</head>
<body>

</body>
</html>
    <h1>Connexion en Cours ...</h1>
heredoc;
UserController::authentification();
});




$app->get('/liste',function(){
  $app = \Slim\Slim::getInstance();
  $rootUri = $app->request()->getRootUri();
  $goListe = $rootUri . '/liste';
   print
    <<< heredoc
    <!DOCTYPE html>
<html>
<head>
  <title>Liste a ajouter</title>
  <link rel="stylesheet" type="text/css" href="styles.css">
</head>
<body>
<h2>La liste des items <h2>
<form action="$goListe" method="POST">

      <input type="text" placeholder="Titre" name="titre" required>
      <input type="text" placeholder="description" name="description">
      <button type="submit">Ajouter la liste</button>
</form>



heredoc;
});

$app->post('/liste',function(){ 
  $app = \Slim\Slim::getInstance();
  $rootUri = $app->request()->getRootUri(); 
  $insertListe = new Liste();
  $insertListe->titre =  $_POST["titre"];
  $insertListe->description =  $_POST["description"];
  $insertListe->token_modification = generationToken(10);
  $insertListe->token_partage = generationToken(10);
  $insertListe->save();

  print
    <<< heredoc
    <!DOCTYPE html>
<html>
<head>
  <title>Liste a ajouter</title>
  <link rel="stylesheet" type="text/css" href="styles.css">
</head>
<body>
<h2>La liste a été ajoutée dont le token de modification est :  $insertListe->token_modification</h2>
  <form action="$rootUri" method="GET">
    <button type="submit">Retour a l'accueil</button>
  </form>
heredoc;
});

$app->post('/updatelistetampon',function(){ 
  $app = \Slim\Slim::getInstance();
  $rootUri = $app->request()->getRootUri(); 
  $token_modification = $_POST['token_modification'];
  $app->response->redirect($rootUri.'/updateliste?token_modification='.$token_modification);
});


$app->get('/updateliste',function(){ 
  $app = \Slim\Slim::getInstance();
  $rootUri = $app->request()->getRootUri(); 
  $token_modification = $_GET['token_modification'];
  $updateliste = $rootUri.'/updateliste'; 
  print
    <<< heredoc
    <!DOCTYPE html>
<html>
<head>
  <title>Liste a ajouter</title>
  <link rel="stylesheet" type="text/css" href="styles.css">
</head>
<body>
  <form action="$updateliste" method="POST">
       <input type="text" placeholder="Nom" name="nom" required>
      <input type="text" placeholder="description" name="description" required>
      <input type="text" placeholder="tarif" name="tarif" required>
      <input type="text" placeholder="url" name="url">
       <input type="hidden" value="$token_modification" name="token_modification">
    <button type="submit">Ajouter items</button>
  </form>
heredoc;
});

$app->post('/updateliste',function(){ 
  $app = \Slim\Slim::getInstance();
  $rootUri = $app->request()->getRootUri();
  $nom = $_POST['nom'];
  $url = $_POST['url'];
  $description = $_POST['description']; 
  $tarif = $_POST['tarif'];
  $token_modification = $_POST['token_modification'];
  $insertItem = new Item();
  $insertItem->nom = $nom;
  $insertItem->descr = $description;
  $insertItem->url = $url;
  $insertItem->tarif = $tarif;
  $insertItem->liste_id = DB::table('liste')->select('no')->where('token_modification','=',$token_modification)->first()->no;
  $insertItem->save();
   print
    <<< heredoc
    <!DOCTYPE html>
<html>
<head>
  <title>Item à ajouter</title>
  <link rel="stylesheet" type="text/css" href="styles.css">
</head>
<body>
<h2>L'item a été ajouté</h2>
  <form action="$rootUri" method="GET">
    <button type="submit">Retour a l'accueil</button>
  </form>
heredoc;

});

$app->get('/partageurl',function(){ 
  $app = \Slim\Slim::getInstance();
  $rootUri = $app->request()->getRootUri(); 
  $token_modification = $_GET['token_modification'];
  $token_partage = $_GET['token_partage'];
  $partage_liste = $rootUri.'/goPartage'; 
  print
    <<< heredoc
    <!DOCTYPE html>
<html>
<head>
  <title>Partage de la liste</title>
  <link rel="stylesheet" type="text/css" href="styles.css">
  form action="$partage_liste" method="POST">
       <input type="text" value="$token_modification" name="token_modification">
    <button type="submit">Partager</button>


</head>
<body>
 
heredoc;
});

$app->post('/partageurl',function(){ 
  $app = \Slim\Slim::getInstance();
  $rootUri = $app->request()->getRootUri();
  $token_modification = $_POST['token_modification'];
  $token_partage = DB::table('liste')->select('token_partage')->where('token_modification','=',$token_modification)->first()->token_partage;

   print
    <<< heredoc
    <!DOCTYPE html>
<html>
<head>
  <title>Item à ajouter</title>
  <link rel="stylesheet" type="text/css" href="styles.css">
</head>
<body>
<h2>Le lien de partage est localhost$rootUri/listepartage?token_partage=$token_partage</h2>
  <form action="$rootUri" method="GET">
    <button type="submit">Retour a l'accueil</button>
  </form>
heredoc;

});


$app->get('/listepartage',function(){
 $app = \Slim\Slim::getInstance();
 $rootUri = $app->request()->getRootUri();
 $token_partage = $_GET['token_partage'];
 $listerecuperation = DB::table('liste')->where('token_partage','=',$token_partage)->first();
   print
    <<< heredoc
    <!DOCTYPE html>
<html>
<head>
  <title>Item à ajouter</title>
  <link rel="stylesheet" type="text/css" href="styles.css">
</head>
<body>
<h2>Liste partagée : </h2>
<p>id : $listerecuperation->no<br/>
titre : $listerecuperation->titre<br/>
description : $listerecuperation->description</p>
heredoc;

});


$app->run();

?>