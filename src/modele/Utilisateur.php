<?php 

namespace wishlist\modele;

class Utilisateur extends \Illuminate\Database\Eloquent\Model{
	protected $table = 'utilisateurs';
	protected $primaryKey = 'id_user';
	public $timestamps = false;

	public function Rang() {
		return $this->belongsTo("wishlist\modele\Rang",
			"id_rang");


	}

	public function liste() {
		return $this->hasMany("wishlist\modele\Liste",
			"id_user");


	}
}


?>