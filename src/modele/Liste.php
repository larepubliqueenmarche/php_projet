<?php 

namespace wishlist\modele;

class Liste extends \Illuminate\Database\Eloquent\Model{
	protected $table = 'liste';
	protected $primaryKey = 'no';
	public $timestamps = false;

	public function Utilisateur() {
		return $this->belongsTo("wishlist\modele\Utilisateur",
			"no");
	}

	public function Item() {
		return $this->hasMany("wishlist\modele\Item",
			"no");
	}


}


?>