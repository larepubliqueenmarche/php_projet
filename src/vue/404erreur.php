<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Erreur 404</title>
	<link rel="stylesheet" type="text/css" href="../css/styles.css">
</head>
<main class="container">
	<div class="row">
		<section class="col">
			<h1>Page non trouvée - ERREUR 404</h1>
			<p>Désolé, la page que vous cherchez n'existe pas !</p><br><hr>
			<p id="quatre"></p>
		</section>
	</div>
</main>