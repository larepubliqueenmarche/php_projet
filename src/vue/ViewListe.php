<?php
require_once __DIR__ . '/vendor/autoload.php';

use wishlist\modele\Item as Item;
use wishlist\modele\Liste as Liste;
use Illuminate\Database\Capsule\Manager as DB;

$db = new DB();

$db->addConnection(parse_ini_file('src/conf/conf.ini'));

$db->setAsGlobal();
$db->bootEloquent();

$id = Item::select('nom','img','descr')->get();
?>
<table>
	<thead>
		<th><strong>Liste des produits<strong></th>
	</thead>
		<tbody>
			<?php  foreach ($id as $item) {
				echo "<tr>";
				echo"<td>$item->nom</td>";
				echo"<td>$item->img</td>";
				echo"<td>$item->descr</td>";
				echo "</tr>";
			}
			?>
		</tbody>
	</table>


