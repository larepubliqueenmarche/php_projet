<?php

require_once __DIR__ . '/vendor/autoload.php';

use wishlist\modele\Item as Item;
use wishlist\modele\Liste as Liste;
use wishlist\modele\Utilisateur as Utilisateur;
use Illuminate\Database\Capsule\Manager as DB;

$db = new DB();

$db->addConnection(parse_ini_file('src/conf/conf.ini'));

$db->setAsGlobal();
$db->bootEloquent();

	// récupération des données du formulaire
	$mail = strip_tags($_POST['mail']);

	// s'il ne manque pas de champs
	if(!empty($mail))
	{

		// vérification de l'email en BDD
		$sql = "SELECT id FROM utilisateur WHERE mail = :mail";

		$query = $db -> prepare($sql);
		$query -> bindValue(":mail", $mail, PDO::PARAM_STR);
		$query -> execute();

		$result = $query -> fetch();

		// si dans la base
		if (!empty($result))
		{
			// génération du token + mise en base
			$token = md5(uniqid(rand(), true));
			$id = $result['id'];

			$sql = "INSERT INTO token VALUES (:id_utilisateur, :token)";

			$query = $db -> prepare($sql);
			$query -> bindValue(":id_utilisateur", $id, PDO::PARAM_INT);
			$query -> bindValue(":token", $token, PDO::PARAM_STR);
			$query -> execute();

			// envoi du "mail"
			echo "<a href='redefinition_mdp.php?id=".$id."&token=".$token."'>LIEN</a>";
			?>
				<a href="redefinition_mdp.php?id=<?= $id ?>&token=<?= $token ?>">LIEN</a>
			<?php
		}
		// sinon
		else
		{
			// message d'erreur
			echo "L'adresse demandée n'est pas dans la base.";
		}
		

			

	}
	// sinon
	else
	{
		// message d'erreur
		echo "Il manque qqch :(";
	}
		

	

		

?>