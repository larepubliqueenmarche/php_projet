<?php
namespace wishlist\controller;

use Illuminate\Database\Capsule\Manager as DB;
use wishlist\modele\Utilisateur as Utilisateur;


/* Création d'un gestionnaire de capsule DB */
$db = new DB();
$db->addConnection(parse_ini_file('src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();
$app = new \Slim\Slim();

class UserController
{
    /* Fonction pour créer un utilisateur */
    public static function creationUtilisateur($nom,$mdp,$confirmer_mdp)
    {
        $app = \Slim\Slim::getInstance();
        $rootUri = $app->request()->getRootUri();
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $password = filter_var($_POST["mot_de_passe"], FILTER_SANITIZE_STRING);
            $password = preg_replace('/\s+/', '', $password);
            $cpassword = filter_var($_POST["mot_de_passe_bis"], FILTER_SANITIZE_STRING);
            $cpassword = preg_replace('/\s+/', '', $cpassword);
            if ($password == $cpassword) {

                if (!isset($passwordErr)) {
                    $newNom = filter_var($_POST["nom"], FILTER_SANITIZE_STRING);
                    $newNom = preg_replace('/\s+/', '', $newNom);
                    $checkingList = Utilisateur::select()->get();
                    $isAvailable = true;
                    foreach ($checkingList as $value) {
                        if (strtolower($value->nom) == strtolower($newNom)) {
                            $isAvailable = false;
                        }
                    }
                    if ($isAvailable) {
                        print($password);
                        print('<br>');
                        print($cpassword);
                        print('<br>');

                        print($_POST['mot_de_passe']);
                        print('<br>');

                        $mdp_hash = password_hash($_POST['mot_de_passe'], PASSWORD_DEFAULT);

                        $insertUser = new Utilisateur();
                        $insertUser->nom = $newNom;
                        $insertUser->mot_de_passe = $mdp_hash;
                        $goLogin = $rootUri . '/';
                         header('refresh: 2;' . $_SERVER['HTTP_REFERER']);
                        $insertUser->save();
                    } else {
                        header('refresh: 2;' . $_SERVER['HTTP_REFERER']);
                    }
                }
            } else {
                print '<br>Erreur - Retour à la page de création de compte dans 2 secondes';
                header('refresh: 2;' . $_SERVER['HTTP_REFERER']);
            }
        }
    }

    public static function authentification()
    {
        $app = \Slim\Slim::getInstance();
        $rootUri = $app->request()->getRootUri();
        $goAccueil = $rootUri . '/accueil';
        $nomPOST = $_POST['nom'];
        $pwClair = $_POST['mot_de_passe'];
        $name_user = DB::table('utilisateurs')->where('nom', $nomPOST)->first();
        if ($isNameOK) {
            print 'Nom d`utilisateur trouvé dans le système<br>';
            $boolCheck = password_verify($pwClair, $name_user->mot_de_passe);
            
            var_dump($boolCheck);
            print('mdp envoyé'.$pwClair.'<br>');
            print($name_user->nom.'<br>');
            print($name_user->mot_de_passe.'<br>');
            print($name_user->id_user.'<br>');
            if ($boolCheck) {
                print("LOLMDR");
                print 'Vérification du password : ' . $boolCheck;
                print '<h1>Identifiants vérifiés</h1>';
                $_SESSION['connexion'][0] = $name_user->id_user;
                $_SESSION['connexion'][1] = $name_user->nom;
                $_SESSION['connexion'][2] = 1;
                header("refresh: 2; url = $goAccueil");
            } else {
                print '<h1>Le mot de passe est faux';
                header('refresh: 2;' . $_SERVER['HTTP_REFERER']);
            }
        } else {
            print '<br>Utilisateur non trouvé';
            header('refresh: 2;' . $_SERVER['HTTP_REFERER']);
        }
    }

}
